package com.revature.myapplication

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.Button
import android.widget.Spinner
import androidx.navigation.fragment.findNavController

import android.widget.*
import androidx.core.os.bundleOf



class Home : Fragment(){

var savedCharNames = mutableListOf<String>()
    var savedChars = mutableListOf<Character>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var root:ViewGroup =
            inflater.inflate(R.layout.fragment_home, null) as ViewGroup;
        var btnView=root.findViewById<Button>(R.id.btnView)
        var btnCreate=root.findViewById<Button>(R.id.btnCreate)
        var spinnerCharacters = root.findViewById<Spinner>(R.id.spnCharacters)
        for(x in LocalStorage.characters)
        {
            savedCharNames.add(x.name)
            savedChars.add(x)
        }
        while(savedCharNames.size<1)
            savedCharNames.add("No Saved Characters")
        var adapt = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, savedCharNames)
        adapt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerCharacters.adapter = adapt


        spinnerCharacters.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>,
                                        view: View, position: Int, id: Long) {
                Toast.makeText(requireContext(),
                     " " +
                            "" + savedCharNames[position], Toast.LENGTH_SHORT).show()
                Log.i("Information", "${savedCharNames[position]}")
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }

        }


        btnView.setOnClickListener {
            LocalStorage.currentChar = spinnerCharacters.selectedItemPosition
            if(LocalStorage.characters.size>0)
            findNavController().navigate(R.id.action_home2_to_characterCreationScreen)
        }


        btnCreate.setOnClickListener {

            LocalStorage.currentChar = -1
            if(LocalStorage.characters.size<5)
            findNavController().navigate(R.id.action_home2_to_characterCreationScreen)


        }
        return root
    }



}