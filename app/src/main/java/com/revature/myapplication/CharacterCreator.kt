package com.revature.myapplication

import android.graphics.Color
import android.graphics.ColorSpace
import kotlin.random.Random

object CharacterCreator {
    var names = arrayOf(
        "Nilsa" ,
    "Tamie" ,
    "Manuela" ,
    "Marietta" ,
    "Caroline" ,
    "Trinity" ,
    "Joellen" ,
    "Joni" ,
    "Brynn" ,
    "Blake",
    "Olen" ,
    "Will" ,
    "Gaye" ,
    "Catheryn" ,
    "Hyun" ,
    "Joy" ,
   "Sharla" ,
    "Eliana" ,
    "Ciara" ,
    "Jillian" ,
    "Li" ,
    "Johnny" ,
    "Elvia",
    "Iluminada",
    "Karmen",
    "Lynne",
    "Billie",
    "Maxine",
    "Claud",
    "Karoline",
    "Aura",
    "Tu",
    "Echo",
    "Nancie",
    "Romelia",
    "Emmaline",
    "Sueann",
    "Janna",
    "Tempie",
    "Ana",
    "Georgie",
    "Melva",
    "Isela",
    "Claribel",
    "Rachelle",
    "Siobhan",
    "Sherman",
    "Britt",
    "Nick",
    "Leigh"
    )
    var colors = arrayOf(
        "Black",
        "Gray",
        "White",
        "Yellow",
        "Amber",
        "Hazel",
        "Green",
        "Blue",
        "Red",
        "Purple",
        "Deep Blue",
        "Orange",
        "Sea Green",
        "Emerald Green"
    )
    fun createRandom(name:String="$%^&*",
                     strength:Int=-1,
                     dexterity:Int=-1,
                     constitution:Int=-1,
                     wisdom:Int=-1,
                     charisma:Int = -1,
                     intelligence:Int=-1,
                     background:String="This person sprung into existence by the will of the gods",
                     hairColor: String="TRANSPARENT",
                     height:Int=-1,
                     bodyType:Int=-1,
                     eyeColor: String="BLACK",
                     charClass:String = "None",
                     weight:Int = -1,
                     age:Int = -1,
                     race:String = "Useless"
    ):Character
    {
        return Character(
            name            =  if(name == "$%^&*") names.random() else name,
            strength        =  if(strength == -1) Random.nextInt(1,8)+10 else strength,
            dexterity       =  if(dexterity == -1) Random.nextInt(1,8)+10 else dexterity,
            constitution    =  if(constitution == -1) Random.nextInt(1,8)+10 else constitution,
            wisdom          =  if(wisdom == -1) Random.nextInt(1,8)+10 else wisdom,
            charisma        =  if(charisma == -1) Random.nextInt(1,8)+10 else charisma,
            intelligence    =  if(intelligence == -1) Random.nextInt(1,8)+10 else intelligence,
            background      =  background,
            hairColor       =  if(hairColor == "TRANSPARENT")
                colors.random()
                else hairColor,
            height          = if(height==-1) Random.nextInt(1,3) else height,
            bodyType        = if(bodyType==-1) Random.nextInt(1,3) else bodyType,
            eyeColor        = if(eyeColor == "TRANSPARENT")
                colors.random()
                else eyeColor,
            charClass       = if(charClass == "None") when(Random.nextInt(1,11))
                {
                    1->"Barbarian"
                    2->"Warlock"
                    3->"Paladin"
                    4->"Bard"
                    5->"Sorcerer"
                    6->"Wizard"
                    7->"Rogue"
                    8->"Fighter"
                    9->"Druid"
                    10->"Monk"
                    11->"Cleric"
                    else -> "Ranger"
            }
                else
                charClass,
            weight          = if(weight==-1) Random.nextInt(1,3) else weight,
            age = if(age<0) Random.nextInt(18,  96) else age,
            race = race
        )
    }
}